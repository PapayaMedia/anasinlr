<?php

define ('LETRAS', 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
define ('NUMEROS', '0123456789');
define ('LAMBDA', '@');

function inicializar() {
  session_start();
  if ($_POST) {
    foreach ($_POST as $key => $value) {
      $_SESSION['formulario'][$key] = $value;
    }
  }
  revisar_formulario();
}

function encabezado() {
  $output = '';
  $output = '<h1>Aut&oacute;matas, gram&aacute;ticas y lenguajes</h1>';
  $output .= '<div class="encabezado">
                <p>
                  <ul>
                    <li><strong>@</strong> es utilizado como LAMBDA.</li>
                  </ul>
                </p>
              </div>';
  return $output;
}

function formulario_gramatica() {
  $output = '';
  $output .= '<form method="post">';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="nombre">Variables:</div>';
      $output .= '<div class="campo"><input type="textfield" name="variables" value="' . $_SESSION['formulario']['variables'] . '" ' . error_campo('variables') . ' /></div>';
      $output .= '<div class="ayuda">
                    <ul>
                      <li>Pueden ir juntas o separadas por espacios o comas.</li>
                    </ul>
                  </div>';
    $output .= '</div>';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="nombre">S&iacute;mbolos del Alfabeto:</div>';


      $output .= '<div class="campo"><input type="textfield" name="simbolos" value="' . $_SESSION['formulario']['simbolos'] . '" ' . error_campo('simbolos') . ' /></div>';
      $output .= '<div class="ayuda">
                    <ul>
                      <li>Pueden ir juntos o separados por espacios o comas.</li>
                    </ul>
                  </div>';
    $output .= '</div>';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="nombre">Reglas:</div>';
      $output .= '<div class="campo"><textarea name="reglas" rows="10" ' . error_campo('reglas') . '>' . $_SESSION['formulario']['reglas'] . '</textarea></div>';
      $output .= '<div class="ayuda">
                    <ul>
                      <li>El formato es <strong>A-@</strong> o <strong>A-@|Aa</strong>.</li>
                      <li>Debe escribirse una regla por cada rengl&oacute;n.</li>
                      <li>El antecedente va separado de sus consecuentes por un <strong>-</strong> (gui&oacute;n).</li>
                      <li>Los consecuentes van separados entre s&iacute; por un <strong>|</strong> (pipe).</li>
                      <li>Puede escribir varios consecuentes para la misma regla utilizando varios renglones, el sistema los reordenar&aacute; al enviar los datos.</li>
                    </ul>
                  </div>';
    $output .= '</div>';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="nombre">Variable Inicial:</div>';
      $output .= '<div class="campo"><input type="textfield" name="inicial" value="' . $_SESSION['formulario']['inicial'] . '" ' . error_campo('inicial') . ' /></div>';
    $output .= '</div>';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="nombre">Cadena a evaluar:</div>';
      $output .= '<div class="campo"><input type="textfield" name="cadena" value="' . $_SESSION['formulario']['cadena'] . '" ' . error_campo('cadena') . ' /></div>';
    $output .= '</div>';
    $output .= '<div class="campo-formulario">';
      $output .= '<div class="campo"><input type="submit" name="enviado" value="Enviar" /></div>';
    $output .= '</div>';
  $output .= '</form>';
  return $output;
}

function mensajes() {
  $output = '';
  if (is_array($_SESSION['mensajes'])) {
    foreach ($_SESSION['mensajes'] as $tipo => $mensajes) {
      $output .= '<div class="mensajes-' . $tipo . '">';
      $output .= '<ul>';
      foreach ($mensajes as $mensaje) {
        $output .= '<li>' . $mensaje . '</li>';
      }
      $output .= '</ul>';
      $output .= '</div>';
    }
  }
  unset($_SESSION['mensajes']);
  if ($output) {
    return '<div class="mensajes">' . $output . '</div>';
  }
  return $output;
}

function crear_mensaje($texto, $tipo = 'normal') {
  $_SESSION['mensajes'][$tipo][] = $texto;
}

function revisar_formulario() {
  if (is_array($_SESSION['formulario'])) {
    verificar_variables();
    verificar_simbolos();
    verificar_inicial();
    verificar_reglas();
    verificar_cadena();
    $_SESSION['generar_tabla'] = FALSE;
    if (!$_SESSION['error-formulario']) {
      crear_mensaje('La informaci&oacute;n de la gram&aacute;tica es correcta.');
      $_SESSION['generar_tabla'] = TRUE;
    }
  }
}

function verificar_variables() {
  $variables = $_SESSION['formulario']['variables'];
  $variables = preg_replace('/,/', ' ', $variables);
  $variables = preg_replace('/ /', '', $variables);
  $_SESSION['formulario']['variables'] = $variables;
  if (strlen($variables)) {
    if (!verificar_caracteres($variables)) {
      error_formulario('variables', 'Hay caract&eacute;res inv&aacute;lidos o repetidos en las variables.  &Uacute;nicamente se permiten letras sin acentos.');
    }
    if (caracter_compartido(LAMBDA, $variables)) {
      error_formulario('variables', 'El caracter <strong>' . LAMBDA . '</strong> (lambda) no puede hacer parte del conjunto de variables.');
    }
  }
  else {
    error_formulario('variables', 'Debe haber al menos 1 variable.');
  }
}

function verificar_simbolos() {
  $simbolos = $_SESSION['formulario']['simbolos'];
  $simbolos = preg_replace('/,/', ' ', $simbolos);
  $simbolos = preg_replace('/ /', '', $simbolos);
  $_SESSION['formulario']['simbolos'] = $simbolos;
  if (strlen($simbolos)) {
    if (!verificar_caracteres($simbolos, LETRAS . NUMEROS)) {
      error_formulario('simbolos', 'Hay caract&eacute;res inv&aacute;lidos o repetidos en los s&iacute;mbolos.  &Uacute;nicamente se permiten letras sin acentos.');
    }
    if (caracter_compartido(LAMBDA, $simbolos)) {
      error_formulario('simbolos', 'El caracter <strong>' . LAMBDA . '</strong> (lambda) no puede hacer parte del conjunto de s&iacute;mbolos.');
    }
    for ($i = 0; $i < strlen($simbolos); $i++) {
      $caracter = substr($simbolos, $i, 1);
      if (caracter_compartido($caracter, $_SESSION['formulario']['variables'])) {
        error_formulario('simbolos', 'Hay al menos un S&iacute;mbolo que tambi&eacute;n pertenece al conjunto de Variables.  Esto no es permitido.');
        return;
      }
    }
  }
}

function verificar_cadena() {
  $cadena = $_SESSION['formulario']['cadena'];
  $cadena = preg_replace('/ /', '', $cadena);
  $_SESSION['formulario']['cadena'] = $cadena;
  if (strlen($cadena)) {
    if (!verificar_caracteres($cadena, $_SESSION['formulario']['simbolos'], TRUE)) {
      error_formulario('cadena', 'En la cadena hay caract&eacute;res que no pertenecen al alfabeto.');
    }
  }
}

function verificar_reglas() {
  $reglas = $_SESSION['formulario']['reglas'];
  if ($reglas) {
    $reglas = explode("\n", $reglas);
    $reglas_final = array();
    foreach ($reglas as $regla) {
      if ($regla) {
        $regla_final = array();
        $regla_datos = explode('-', $regla);
        $variable = trim($regla_datos[0]);
        foreach (explode('|', $regla_datos[1]) as $texto) {
          $texto = trim(preg_replace('/ /', '', $texto));
          if ($texto) {
            if (!in_array($texto, $regla_final[$variable])) {
              $regla_final[$variable][] = $texto;              
            }
          }
        }
        $reglas_final = array_merge_recursive($reglas_final, $regla_final);
      }
    }
    foreach ($reglas_final as $key => $value) {
      if (strlen($key) == 1) {
        if (!caracter_compartido($key, $_SESSION['formulario']['variables'])) {
          error_formulario('reglas', 'La llave <strong>' . $key . '</strong> no est&aacute; definida en el conjunto de Variables.');
        }
      }
      else {
        error_formulario('reglas', 'Las llaves de las reglas deben tener 1 y s&oacute;lo 1 variable.');
      }
      foreach ($value as $regla) {
        if (!verificar_caracteres($regla, $_SESSION['formulario']['variables'] . $_SESSION['formulario']['simbolos'] . LAMBDA, TRUE)) {
          error_formulario('reglas', 'La regla <strong>' . $regla . '</strong> tiene caracteres que no est&aacute;n definidos ni en el conjunto de Variables ni en el Alfabeto ni son el caracter <strong>' . LAMBDA . '</strong> (lambda).');
        }
      }
    }
    $_SESSION['formulario']['reglas'] = '';
    ksort($reglas_final);
    foreach ($reglas_final as $key => $value) {
      sort($value);
      if ($key == $_SESSION['formulario']['inicial']) {
        $_SESSION['formulario']['reglas'] .= $key . '-' . implode('|', $value) . "\n";
      }
    }
    foreach ($reglas_final as $key => $value) {
      sort($value);
      if ($key != $_SESSION['formulario']['inicial']) {
        $_SESSION['formulario']['reglas'] .= $key . '-' . implode('|', $value) . "\n";
      }
    }
  }
}

function verificar_inicial() {
  $inicial = $_SESSION['formulario']['inicial'];
  if ($l = strlen($inicial)) {
    if ($l == 1) {
      if (!caracter_compartido($inicial, $_SESSION['formulario']['variables'])) {
        error_formulario('inicial', 'La Variable Inicial debe pertenecer al conjunto de Variables.');
      }
    }
    else {
      error_formulario('inicial', '&Uacute;nicamente debe haber una Variable Inicial.');
    }
  }
  else {
    error_formulario('inicial', 'Debe declarar una Variable Inicial.');
  }
}

function error_formulario($campo = '', $texto = '') {
  $_SESSION['error-formulario'][$campo] = TRUE;
  crear_mensaje($texto, 'error');
}

function error_campo($campo) {
  if ($_SESSION['error-formulario'][$campo]) {
    unset($_SESSION['error-formulario'][$campo]);
    return 'class="campo-error"';
  }
}

function verificar_caracteres($texto, $verificar = LETRAS, $repetidos = FALSE) {
  //print $texto . ' :: ' . $verificar;
  for ($i = 0; $i < strlen($texto); $i++) {
    $caracter = substr($texto, $i, 1);
    if (strpos($verificar, $caracter) === FALSE || (!$repetidos && strpos(substr($texto, 0, $i), $caracter) !== FALSE)) {
      return FALSE;
    }
  }
  return TRUE;
}

function caracter_compartido($aguja, $pajar) {
  return strpos($pajar, $aguja) !== FALSE;
}

function generar_transiciones() {
  $_SESSION['transiciones'] = array();
  generar_estados('$-' . $_SESSION['formulario']['inicial'] . '-0', $_SESSION['transiciones']);
}

function generar_tabla() {
  $simbolos = $_SESSION['formulario']['simbolos'];
  $variables = $_SESSION['formulario']['variables'];
  $reglas_inicial = get_reglas($_SESSION['formulario']['inicial']);
  $_SESSION['tabla'] = array();
  foreach ($_SESSION['transiciones'] as $key => $value) {
    $index = get_array_pos($key, $_SESSION['transiciones']);
    // Generar desplazamientos y retrocesos
    for ($i = 0; $i < strlen($simbolos); $i++) {
      $simbolo = substr($simbolos, $i, 1);
      if (array_key_exists($simbolo, $_SESSION['transiciones'][$key])) {
        $_SESSION['tabla'][$index][$simbolo] = 'd|' . get_array_pos($_SESSION['transiciones'][$key][$simbolo], $_SESSION['transiciones']);
      }
      elseif (!$value) {
        $regla = explode('-', $key);
        if (!in_array($regla[0], array('$', $_SESSION['formulario']['inicial']))) {
          $_SESSION['tabla'][$index][$simbolo] = 'r|' . $key;
        }
      }
    }
    // Generar datos en columnas de variables
    for ($i = 0; $i < strlen($variables); $i++) {
      $variable = substr($variables, $i, 1);
      if (array_key_exists($variable, $_SESSION['transiciones'][$key])) {
        $_SESSION['tabla'][$index][$variable] = 'e|' . get_array_pos($_SESSION['transiciones'][$key][$variable], $_SESSION['transiciones']);
      }
    }
    // Generar detos en columna FDC
    if ($key == '$-' . $_SESSION['formulario']['inicial'] . '-1') {
      $_SESSION['tabla'][$index]['fdc'] = 'a';
    }
    foreach ($reglas_inicial as $value) {
      $regla = $_SESSION['formulario']['inicial'] . '-' . $value . '-' . strlen($value);
      if ($key == $regla) {
        $_SESSION['tabla'][$index]['fdc'] = 'r|' . $regla;
      }
    }
  }
}

function dibujar_tabla() {
  $simbolos = $_SESSION['formulario']['simbolos'];
  $variables = $_SESSION['formulario']['variables'];
  $output = '<table border="1">';
  $output .= '<tr align="center"><th>Estado</th><th>ID</th>';
  for ($i = 0; $i < strlen($simbolos); $i++) {
    $output .= '<th>' . substr($simbolos, $i, 1) . '</th>';
  }
  $output .= '<th>FDC</th>';
  for ($i = 0; $i < strlen($variables); $i++) {
    $output .= '<th>' . substr($variables, $i, 1) . '</th>';
  }
  $output .= '</tr>';
  foreach ($_SESSION['tabla'] as $key => $value) {
    $output .= '<tr align="center"><td>' . format_estado(get_array_key($key - 1, $_SESSION['transiciones'])) . '</td><td>' . $key . '</td>';
    for ($i = 0; $i < strlen($simbolos); $i++) {
      $simbolo = substr($simbolos, $i, 1);
      $output .= '<td>' . get_celda($_SESSION['tabla'][$key][$simbolo]) . '</td>';
    }
    $output .= '<td>' . get_celda($_SESSION['tabla'][$key]['fdc']) . '</td>';
    for ($i = 0; $i < strlen($variables); $i++) {
      $variable = substr($variables, $i, 1);
      $output .= '<td>' . get_celda($_SESSION['tabla'][$key][$variable]) . '</td>';
    }
    $output .= '</tr>';
  }
  $output .= '</table>';
  return $output;
}

function get_celda($dato) {
  $output = '';
  $dato = explode('|', $dato);
  switch ($dato[0]) {
    case 'a':
      $output = 'aceptar';
      break;
    case 'd':
      $output = 'desplazar ' . $dato[1];
      break;
    case 'e':
      $output = $dato[1];
      break;
    case 'r':
      $regla = explode('-', $dato[1]);
      $output = $regla[0] . ' &rarr; ' . $regla[1];
      break;
  }
  return $output;
}

function generar_estados($key, &$tabla) {
  if (!array_key_exists($key, $tabla)) {
    $tabla[$key] = array();
    $regla = explode('-', $key);
    if (strlen($regla[1]) != $regla[2]) {
      $siguiente = substr($regla[1], $regla[2], 1);
      $next = $regla[0] . '-' . $regla[1] . '-' . ($regla[2] + 1);
      $tabla[$key][$siguiente] = $next;
      generar_estados($next, $tabla);
      $reglas = get_reglas($siguiente);
      foreach($reglas as $value) {
        $primer = substr($value, 0, 1);
        $next = $siguiente . '-' . $value . '-1';
        $tabla[$key][$primer] = $next;
        generar_estados($next, $tabla);
      }
    }
  }
}

function get_reglas($var) {
  $reglas = array();
  $reglas_formulario = $_SESSION['formulario']['reglas'];
  if ($reglas_formulario) {
    $reglas_formulario = explode("\n", $reglas_formulario);
    foreach ($reglas_formulario as $regla) {
      if ($regla) {
        $regla_datos = explode('-', $regla);
        $variable = trim($regla_datos[0]);
        if ($variable == $var) {
          foreach (explode('|', $regla_datos[1]) as $texto) {
            $texto = trim(preg_replace('/ /', '', $texto));
            if ($texto) {
              if (!in_array($texto, $reglas)) {
                $reglas[] = $texto;              
              }
            }
          }
        }
      }
    }
  }
  return $reglas;
}

function get_array_pos($search, $array) {
  $i = 1;
  foreach ($array as $key => $value) {
    if ($key == $search) {
      return $i;
    }
    $i++;
  }
  return 0;
}

function get_array_key($key, $array) {
  $keys = array_keys($array);
  return $keys[$key];
}

function format_estado($estado) {
  $output = '';
  $estado = explode('-', $estado);
  $var = $estado[0];
  if ($var == '$') {
    $var = $_SESSION['formulario']['inicial'] . "'";
  }
  $output .= $var . ' &rarr; ';
  for ($i = 0; $i < strlen($estado[1]); $i++) {
    if ($i == $estado[2]) {
      $output .= '&middot;';
    }
    $output .= substr($estado[1], $i, 1);
  }
  if ($i == $estado[2]) {
    $output .= '&middot;';
  }
  return $output;
}

function analizar_palabra() {
  $error = FALSE;
  $output = '<table border="1">';
  $tabla = $_SESSION['tabla'];
  $i = 0;
  $simbolo = leer($i);
  $pila = array();
  $output .= add_renglon($pila, $i);
  $estado = 1;
  array_push($pila, $estado);
  $valor = explode('|', $tabla[$estado][$simbolo]);
  while ($valor[0] != 'a') {
    $output .= add_renglon($pila, $i);
    switch ($valor[0]) {
      case 'd':
        array_push($pila, $simbolo);
        $estado = $valor[1];
        array_push($pila, $estado);
        $i++;
        $simbolo = leer($i);
        break;
      case 'r':
        $regla = explode('-', $valor[1]);
        for ($j = 1; $j <= strlen($regla[1]) * 2; $j++) {
          array_pop($pila);
        }
        $index = count($pila) - 1;
        $estado = $pila[$index];
        $simbolo = $regla[0];
        array_push($pila, $simbolo);
        $temp = explode('|', $tabla[$estado][$simbolo]);
        $estado = $temp[1];
        array_push($pila, $estado);
        $simbolo = leer($i);
        break;
      default:
        $error = TRUE;
    }
    if ($error) {
      $valor[0] = 'a';
    }
    else {
      $valor = explode('|', $tabla[$estado][$simbolo]);
    }
  }
  $output .= add_renglon($pila, $i);
  if ($error) {
    $output .= '<tr><td></td><td class="cadena-false">La cadena no pertenece a la gram&aacute;tica.</td></tr>';
  }
  else {
    $output .= '<tr><td>vac&iacute;a</td><td class="cadena-true">La cadena pertenece a la gram&aacute;tica.</td></tr>';
  }
  $output .= '</table>';
  return $output;
}

function add_renglon($pila, $i) {
  $cadena = $_SESSION['formulario']['cadena'];
  $output = '<tr><td align="left">' . convertir_pila($pila) . '</td><td align="right">' . substr($cadena, $i, strlen($cadena)-$i) . '</td></tr>';
  return $output;
}

function convertir_pila($pila) {
  $output = '';
  $i = 0;
  if ($pila) {
    foreach($pila as $elemento) {
      $output .= (($i++ % 2 == 0) ? '<span class="especial">' . $elemento . '</span> ' : '<strong>' . $elemento . '</strong> ');
    }
  }
  else {
    $output .= 'vac&iacute;a';
  }
  return $output;
}

function leer($i) {
  $simbolo = 'fdc';
  $cadena = $_SESSION['formulario']['cadena'];
  if ($i < strlen($cadena)) {
    $simbolo = substr($cadena, $i, 1);
  }
  return $simbolo;
}
